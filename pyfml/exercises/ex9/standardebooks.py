import requests_html

S = requests_html.HTMLSession()

base_url = "https://standardebooks.org"


def get_items(page=1):
    resp = S.get("https://standardebooks.org/ebooks?page={}".format(page))
    return resp.html.xpath("//main/ol/li")


def main():
    page = 1
    while True:
        lis = get_items(page)
        if not lis:
            break

        for li in lis:
            author = li.xpath('//p[@class="author"]/a/text()')[0]
            title_a = li.xpath("//p[not(@class)]/a")[0]
            title = title_a.xpath("//text()")[0]
            url = title_a.xpath("//@href")[0]
            print(
                "{:<25} - {:<30} - {:<80} ".format(
                    author, title, base_url + url
                )
            )

        page = page + 1


if __name__ == "__main__":
    main()
